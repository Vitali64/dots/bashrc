#
# THE BEST SHELL IS BASH
#
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Variables

##Parameters
##----
###Path of Ble.SH and if it's going to be used instead of GNU Readline (yes,no)
BLEDOTSH=$HOME/ble.sh/ble.sh/out/blesh
BLEACTIVE=no
###If the prompt shows all info or just the path and the git branch (yes,no)
P_MINIMALIST=no

##Colors
##----
TEXT_BOLD_WHITE='\[\033[1;37m\]'
TEXT_BOLD_GREEN='\[\033[1;32m\]'
TEXT_BOLD_BLUE='\[\033[1;34m\]'
TEXT_ORANGE='\[\033[0;33m\]'
TEXT_BOLD_RED='\[\033[1;31m\]'
TEXT_WHITE='\[\033[0;37m\]'

##Other things
##----
P_DATE='[  ] $(date)'
P_USER='[  ] $USER'
P_GIT='[  ]$(git branch 2> /dev/null | sed -e "/^[^*]/d" -e "s/*\(.*\)/\1/")'
P_PATH="${TEXT_ORANGE}  ${TEXT_BOLD_WHITE}\W ${TEXT_WHITE}"

error_check()
{
    if [ $? != 0 ]; then
        echo ":(\n"
    else
        echo ""
    fi
}
# Aliases
alias d='startx'
alias v="nvim"
alias edit="sudo nvim"
alias ref="source ~/.bashrc"
alias nodeewm='killall xinit'
alias ps="doas pacman -S --needed "
alias s="doas "
alias dot='cd $HOME/.config/ && ls'
alias cleat="clear"
alias cls="clear"
alias clr="clear"
alias ll="ls -la"
alias l="ls"
alias cool="pfetch"
alias t="tmux"
alias battery="cat /sys/class/power_supply/BAT0/capacity"
alias sudo="echo 'Did you mean doas?' && doas"

# Syntax highlighting
if [ "$BLEACTIVE" = "yes" ]; then
    source $BLEDOTSH
    echo "Using BLE.SH, Bash Line Editor"
fi
if [ "$BLEACTIVE" = "no" ]; then
    echo "Using GNU Readline"
fi

# Define $TERM for 256 colors support


# Start commands at bootup to look cool
clear
tmux a 2> /dev/null
echo -e "
█ █ █ █ █ █ █ █ █ █ █ █ █
 █ █ █ █ █ █ █ █ █ █ █ █ 
█ █ █ █ █ █ █ █ █ █ █ █ █
 █ █ █ █ █ █ █ █ █ █ █ █
█ █ █ █ █ █ █ █ █ █ █ █ █
" |lolcat
echo -e "Welcome back to GNU Bash ! \nThe Bourne-Again SHell !" |lolcat

# Prompt
if [ "$TERM" != "linux" ]; then
    if [ "$P_MINIMALIST" = "no" ]; then
        export PS1="${TEXT_BOLD_GREEN}${P_DATE}${TEXT_BOLD_WHITE} | ${TEXT_BOLD_BLUE}${P_USER}\n${TEXT_BOLD_RED}${P_GIT}${P_PATH}"
        TERM=xterm-256color
    fi
    if [ "$P_MINIMALIST" = "yes" ]; then
        export PS1="${TEXT_BOLD_RED}${P_GIT}${P_PATH}"
        TERM=xterm-256color
    fi
fi
# if TTY, ignore all Prompt settings and instead, go to a TTY-friendly PS1
if [ "$TERM" = "linux" ]; then
    PS1="$(error_check)[ TTY ] ${TEXT_BOLD_WHITE} ${P_GIT} | $USER: \W -> ${TEXT_WHITE}"
    setfont /usr/share/kbd/consolefonts/ter-m20b.psf.gz
fi

