# My Bash RC

It's a simple bashrc with autocompletion (*cough ble.sh)

You will need [ble.sh](https://github.com/akinomyoga/ble.sh) for autocompletion and to modify the "BLEDOTSH" variable to the correct path and the "BLEACTIVE" variable.

Normal :

![Normal](./normal.png)

Minimal :

![Minimal](./minimalist.png)

TTY :

![TTY Bash](./tty.png)

Startup :

![Startup](./startup.png)
